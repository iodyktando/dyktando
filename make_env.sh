#!/bin/bash

quick_cd() {

	if ! shopt -q cdable_vars
	then
		shopt -s cdable_vars
	fi

	p=`pwd`

	cd dyktando
	d=`pwd`

	cd templates/dyktando
	td=`pwd`
	cd ../../

	cd static
	s=`pwd`

	cd css
	sc=`pwd`

	cd ../js
	sj=`pwd`
	cd ../../

	cd ../

	cd project
	pp=`pwd`
	cd ../

	# Teraz możesz przejść do katalogu $td tak: cd td
}

short_ps() {
	#PS1="(`basename $VIRTUAL_ENV`)\u@\h \W$ "
	PS1="\u@\h \W$ "
	if [ -n "$VIRTUAL_ENV" ]; then
		PS1="(`basename $VIRTUAL_ENV`)$PS1"
	fi
}

print_usage() {
	echo -e "Usage . make_env [cd|ps|all]"
	echo -e "\tcd  \tTworzy zmienne środowiskowe do szybkiego przeskakiwania"
	echo -e "\t    \tpomiędzy katalogami"
	echo -e "\tps  \tTworzy krótkie PS1"
	echo -e "\tall \tRównoważne użyciu obu opcji: cd i ps"
}

if [ $# -eq 0 ]; then
	print_usage
fi

while [ $# -gt 0 ]; do
	case $1 in
		cd)
			quick_cd
			;;
		ps)
			short_ps
			;;
		all)
			quick_cd
			short_ps
			;;
		*)
			print_usage
			;;
	esac
	shift 1
done
