from django import forms
from django.contrib.auth.models import User
from django.db import transaction
from dyktando.models import *

class userRegisterForm(forms.Form):
        username = forms.CharField(label = 'Login', max_length=30)
       	password = forms.CharField(label = 'Haslo', widget=forms.PasswordInput)

        def save(self):
                with transaction.atomic():
                       	username = self.cleaned_data['username']
                        password = self.cleaned_data['password']
			user = User.objects.create_user(username)
			musician = Musician.create(user)
			musician.save()
			user.musician = musician
			user.set_password(password)
                        return user.save()

