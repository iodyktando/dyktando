from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Musician(models.Model):
	user = models.OneToOneField(User)
	liczba_dyktand = models.IntegerField(default=0)
	liczba_interwalow = models.IntegerField(default=0)
	ocena_dyktand = models.FloatField(default=0.0)
	poprawne_interwaly = models.IntegerField(default=0)
	bledne_interwaly = models.IntegerField(default=0)
	
	@classmethod
	def create(cls, user):
		musician = cls(user=user)
		return musician
