from django.shortcuts import render
from django.http import HttpResponse
from django.template import Context, loader
from django.contrib.auth import views
from dyktando.forms import *
from django.contrib.auth.models import User
from dyktando.models import *

def main(request):
	return render(request, "dyktando/intro.html")
	
def rytmika(request):
	return render(request, "dyktando/rytmika.html")

def dyktando(request):
	return render(request, "dyktando/dyktando.html")

def interwaly(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			u = User.objects.get(username = request.user.username)
			u.musician.liczba_interwalow += 1
			u.musician.poprawne_interwaly += (int)(request.POST['l_poprawnych'])
			u.musician.bledne_interwaly += (int)(request.POST['l_blednych'])
			u.musician.save()
			u.save()
		else:
			print "niezalogowany"
		return render(request, "dyktando/profile.html")
	
	return render(request, "dyktando/interwaly.html")

def teoria(request):
	return render(request, "dyktando/teoria.html")

def logout_success(request):
	return render(request, "registration/logout_success.html")

def profile(request):
	return render(request, "dyktando/profile.html")
	
def info(request):
	return render(request, "dyktando/info.html")

def intro(request):
	return render(request, "dyktando/intro.html")

def register(request):
	if request.method == 'POST':
        	form = userRegisterForm(request.POST)
		u = User.objects.filter(username = request.POST["username"])
		if u:
			return render(request, "registration/register_fail.html")
               	if form.is_valid():
			form.save()
			return render(request, "registration/register_success.html")
			
	form = userRegisterForm()
	
	return render(request, "registration/register.html", {"form" : form,})
