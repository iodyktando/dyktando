from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	
    url(r'^logout/$', 'django.contrib.auth.views.logout',
                         {'next_page': '/logout_success/'}),
    url(r'^logout_success/$', "dyktando.views.logout_success"),
    url(r'^admin/', include(admin.site.urls)),
	url(r'^dyktando/$', "dyktando.views.dyktando"),
	url(r'^interwaly/$', "dyktando.views.interwaly"),
	url(r'^teoria/$', "dyktando.views.teoria"),
	url(r'^rytmika/$', "dyktando.views.rytmika"),
	url(r'^$', "dyktando.views.main"),
	url('^', include('django.contrib.auth.urls')),
	url(r'^register/$', "dyktando.views.register", name='register'),
	url(r'^accounts/profile/$', "dyktando.views.profile", name='profile'),
]
